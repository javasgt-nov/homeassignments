package january22assgnmntNo2;

import java.util.ArrayList;

public class District {
	
	private String title, city;
	private int districtID;
	private ArrayList<Officer> officersInTheDistrict = new ArrayList<Officer>();
	private ArrayList<Lawyer> lawyersInTheDistrict = new ArrayList<Lawyer>();
	private ArrayList<Person> personsInTheDistrict = new ArrayList<Person>();

	public District() {

	}
	
	public District(String title, String city, int districtID) {
		this.city = city;
		this.districtID = districtID;
		this.title = title;
		this.city = city;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getDistrictID() {
		return districtID;
	}

	public void setDistrictID(int districtID) {
		this.districtID = districtID;
	}

	@Override
	public String toString() {
		return "The title: " + this.title + System.lineSeparator() + "City: " + this.city + System.lineSeparator()
				+ "District ID: " + this.districtID + System.lineSeparator() + "There are "
				+ this.officersInTheDistrict.size() + " officers in the district" + System.lineSeparator()
				+ "There are " + this.lawyersInTheDistrict.size() + " lawyers in the district" + System.lineSeparator();
	}

	public void addOfficer(Officer officer){
		this.officersInTheDistrict.add(officer);
		
	}
	
	public void removerOfficer(Officer officer) {
		this.officersInTheDistrict.remove(officer);
	}

	public void addLawyer(Lawyer lawyer){
		this.lawyersInTheDistrict.add(lawyer);
		
	}
	
	public ArrayList<Lawyer> getLawyersInTheDistrict() {
		return lawyersInTheDistrict;
		
	}
	
	public void setLawyersInTheDistrict(ArrayList<Lawyer> lawyersInTheDistrict) {
		this.lawyersInTheDistrict = lawyersInTheDistrict;
	}
	
	public float calculateAvgLevellInDistrict() {
		float levelSum = 0;
		for (int i = 0; i < this.officersInTheDistrict.size(); i++)
			levelSum += this.officersInTheDistrict.get(i).calculateLevel();

		return levelSum / this.officersInTheDistrict.size();
	}

	public ArrayList<Officer> getOfficersInTheDistrict() {
		return officersInTheDistrict;
	}

	public void setOfficersInTheDistrict(ArrayList<Officer> officersInTheDistrict) {
		this.officersInTheDistrict = officersInTheDistrict;
	}

	public void addPerson(Person person){
		this.personsInTheDistrict.add(person);
		
	}
	
	public ArrayList<Person> getPersonsInTheDistrict() {
		return personsInTheDistrict;
		
	}
	
	public void setPersonsInTheDistrict(ArrayList<Person> personsInTheDistrict) {
		this.personsInTheDistrict = personsInTheDistrict;
	}
	
}