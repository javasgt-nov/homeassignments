package january22assgnmntNo2;

import java.util.ArrayList;
import java.util.List;

public class Execution {

	public static void main(String[] args) {
				
		Officer officer1 = new Officer("Freddie", "Mercury", "District 1", 829, 73);
		Officer officer2 = new Officer("Axel", "Rose", "District 1", 42, 24);
		Officer officer3 = new Officer("John", "Bon Jovi", "District 1", 66, 102);
		Officer officer4 = new Officer("Alice", "Cooper", "District 2", 331, 12);
		Officer officer5 = new Officer("Kurt", "Cobain", "District 2", 14, 28);
		Officer officer6 = new Officer("Ozzy", "Osbourne", "District 2", 808, 54);
		Officer officer7 = new Officer("Steven", "Tyler", "District 2", 77, 5);
		
		Lawyer lawyer1 = new Lawyer("Queen", "Elizabeth II", 1, 32);
		Lawyer lawyer2 = new Lawyer("Oprah", "Winfrey", 57, 100);
		Lawyer lawyer3 = new Lawyer("Gamora", "Zen", 22, 721);
		
		District district1 = new District("District 1", "Manila", 27);
		District district2 = new District("District 2", "Beijing", 892);

			district1.addOfficer(officer1);
			district1.addOfficer(officer2);
			district1.addOfficer(officer3);
			district1.addLawyer(lawyer1);
			district1.addLawyer(lawyer2);
			district2.addOfficer(officer4);
			district2.addOfficer(officer5);
			district2.addOfficer(officer6);
			district2.addOfficer(officer7);
			district2.addLawyer(lawyer3);
		
		System.out.println(district1);
		officer1.describe();
		System.out.println(officer1);
		officer2.describe();
		System.out.println(officer2);
		officer3.describe();
		System.out.println(officer3);
		System.out.print(System.lineSeparator());
		lawyer1.describe();
		System.out.println(lawyer1);
		lawyer2.describe();
		System.out.println(lawyer2);
		System.out.print(System.lineSeparator());
		System.out.println(district2);
		officer4.describe();
		System.out.println(officer4);
		officer5.describe();
		System.out.println(officer5);
		officer6.describe();
		System.out.println(officer6);
		officer7.describe();
		System.out.println(officer7);
		System.out.print(System.lineSeparator());
		lawyer3.describe();
		System.out.println(lawyer3);
		System.out.print(System.lineSeparator());
		
		ArrayList<Officer> officersInTheFirstDistrict = district1.getOfficersInTheDistrict();
		ArrayList<Officer> officersInTheSecondDistrict = district2.getOfficersInTheDistrict();
		
		District commonDistrict = new District("Common district", "Tokyo", 88);
		commonDistrict.getOfficersInTheDistrict().addAll(officersInTheFirstDistrict);
		commonDistrict.getOfficersInTheDistrict().addAll(officersInTheSecondDistrict);
		System.out.println("The average level in the districts is: " + commonDistrict.calculateAvgLevellInDistrict());
		
		List<District> districts = new ArrayList<District>();
		districts.add(district1);
		districts.add(district2);
		
		List<Person> personsInTheFirstDistrict = new ArrayList<Person>();
		personsInTheFirstDistrict.add(officer1);
		personsInTheFirstDistrict.add(officer2);
		personsInTheFirstDistrict.add(officer3);
		personsInTheFirstDistrict.add(lawyer1);
		personsInTheFirstDistrict.add(lawyer2);
		
		List<Person> personsInTheSecondDistrict = new ArrayList<Person>();
		personsInTheSecondDistrict.add(officer4);
		personsInTheSecondDistrict.add(officer5);
		personsInTheSecondDistrict.add(officer6);
		personsInTheSecondDistrict.add(officer7);
		personsInTheSecondDistrict.add(lawyer3);
		
		if (personsInTheFirstDistrict.size() == personsInTheSecondDistrict.size())
			System.out.println("Both districts have the same amount of persons in them");
		else
			System.out.println((personsInTheFirstDistrict.size() > personsInTheSecondDistrict.size())
					? "District 1 has the highest amount of persons"
					: "District 2 has the highest amount of persons");
		
	}

}
