package december21;

import java.util.*;

public class Branching2_3 {

	public static void main(String[] args) {

		Scanner myScanner = new Scanner(System.in);

		System.out.println("Enter the first number:");
		int x = Integer.parseInt(myScanner.nextLine());

		System.out.println("Enter operator +, -, /, *, %, p, b or s: ");
		char operator = myScanner.nextLine().charAt(0);

		System.out.println("Enter the second number:");
		int y = myScanner.nextInt();
		myScanner.close();

		int solution = 0;
		String solution1 = "";
		boolean numberAsResult = true;

		switch (operator) {

		case '+':
			numberAsResult = true;
			solution = x + y;
			break;

		case '-':
			numberAsResult = true;
			solution = x - y;
			break;

		case '/':
			numberAsResult = true;
			solution = x / y;
			break;

		case '*':
			numberAsResult = true;
			solution = x * y;
			break;

		case '%':
			numberAsResult = true;
			solution = x % y;
			break;

		case 'p':
			numberAsResult = false;
			solution1 = (x + " and " + y);
			break;

		case 'b':
			numberAsResult = false;
			if (x > y) {
				solution1 = (x + " is bigger than " + y);
			} else if (x < y) {
				solution1 = (y + " is bigger than " + x);
			} else
				solution1 = (x + " equals " + y);
			break;

		case 's':
			numberAsResult = false;
			if (x < y) {
				solution1 = (x + " is smaller than " + y);
			} else if (x > y) {
				solution1 = (y + " is smaller than " + x);
			} else
				solution1 = (x + " equals " + y);
			break;

		default:
			System.out.println("The operator is incorrect! Run the program again!");
			return;

		}

		if (numberAsResult == true) {
			System.out.println("Result is: " + solution);
		} else
			System.out.println("Result is: " + solution1);

	}

}
