package december21;

import java.util.*;

public class Branching2_1 {

	public static void main(String[] args) {

		int dayNumber;
		String typeOfDay = "";
		Scanner myScanner = new Scanner(System.in);

		System.out.println("Enter the day number in the week: ");
		dayNumber = Integer.parseInt(myScanner.nextLine());
		myScanner.close();

		if (dayNumber < 1) {
			System.out.println("Day number has to be above 0!");
			return;
		}
		if (dayNumber > 7) {
			System.out.println("Day number must be less than 8!");
			return;
		}

		switch (dayNumber) {

		case 1, 2, 3, 4, 5:
			typeOfDay = "Working day";
			break;

		case 6, 7:
			typeOfDay = "Holiday";
			break;

		}

		System.out.println("This day is a " + typeOfDay);

	}

}
