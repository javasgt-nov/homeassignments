package december21;

import java.util.*;

public class Branching2_2 {

	public static void main(String[] args) {

		char grade;
		String evaluation = "";
		Scanner myScanner = new Scanner(System.in);

		System.out.println("Enter the grade: ");
		grade = myScanner.nextLine().charAt(0);
		myScanner.close();

		switch (grade) {

		case 'A', 'B':
			evaluation = "Perfect! You are so clever!";
			break;

		case 'C':
			evaluation = "Good! But You can do better!";
			break;

		case 'D', 'E':
			evaluation = "It is not good! You should study!";
			break;

		case 'F':
			evaluation = "Fail! You need to repeat the exam!";
			break;

		default:
			System.out.println("The grade is incorrect!");
			break;

		}

		System.out.println(evaluation);

	}

}
