package january22;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Execution {
	
	public static void main(String[] args) {
				
		Officer officer1 = new Officer("Freddie", "Mercury", "District 1", 829, 73);
		Officer officer2 = new Officer("Axel", "Rose", "District 1", 42, 24);
		Officer officer3 = new Officer("John", "Bon Jovi", "District 1", 66, 102);
		Officer officer4 = new Officer("Alice", "Cooper", "District 2", 331, 12);
		Officer officer5 = new Officer("Kurt", "Cobain", "District 2", 14, 28);
		Officer officer6 = new Officer("Ozzy", "Osbourne", "District 2", 808, 54);
		Officer officer7 = new Officer("Steven", "Tyler", "District 2", 77, 5);
		
		District district1 = new District("District 1", "Manila", 27);
		District district2 = new District("District 2", "Beijing", 892);

			district1.addOfficer(officer1);
			district1.addOfficer(officer2);
			district1.addOfficer(officer3);
			district2.addOfficer(officer4);
			district2.addOfficer(officer5);
			district2.addOfficer(officer6);
			district2.addOfficer(officer7);
				
		Lawyer lawyer1 = new Lawyer("Queen", "Elizabeth II", 1, 32);
		Lawyer lawyer2 = new Lawyer("Oprah", "Winfrey", 57, 100);
		Lawyer lawyer3 = new Lawyer("Gamora", "Zen", 22, 721);
		
		System.out.println(district1);
		officer1.describe();
		System.out.println(officer1);
		officer2.describe();
		System.out.println(officer2);
		officer3.describe();
		System.out.println(officer3);
		System.out.print(System.lineSeparator());
		System.out.println(district2);
		officer4.describe();
		System.out.println(officer4);
		officer5.describe();
		System.out.println(officer5);
		officer6.describe();
		System.out.println(officer6);
		officer7.describe();
		System.out.println(officer7);
		System.out.print(System.lineSeparator());
		lawyer1.describe();
		System.out.println(lawyer1);
		lawyer2.describe();
		System.out.println(lawyer2);
		lawyer3.describe();
		System.out.println(lawyer3);
		System.out.print(System.lineSeparator());
		
		List<Lawyer> lawyers = new ArrayList<Lawyer>();
		lawyers.add(lawyer1);
		lawyers.add(lawyer2);
		lawyers.add(lawyer3);
		
//		How to calculate total number of crimes? Sorry, but I just couldn't figure out what to do next.
		
		Lawyer totalCrimesSum = null;
			Iterator<Lawyer> iterator2 = lawyers.iterator();
		while (iterator2.hasNext()) {
			Lawyer currentLawyer2 = iterator2.next();
			currentLawyer2.getHelpedInCrimesSolving();
		}
	
//		System.out.println(totalCrimesSum.getHelpedInCrimesSolving());
		
		Lawyer bestLawyer = null;
				Iterator<Lawyer> iterator = lawyers.iterator();
		while (iterator.hasNext()) {
			Lawyer currentLawyer = iterator.next();
			if (bestLawyer == null || bestLawyer.getHelpedInCrimesSolving() 
					< currentLawyer.getHelpedInCrimesSolving())
				bestLawyer = currentLawyer;
		}

		System.out.println(bestLawyer.getName() + " has helped the most to solve crimes");

		
	}

}
