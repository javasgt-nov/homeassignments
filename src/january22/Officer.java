package january22;

public class Officer extends Person {

	private int officerID, crimesSolved;

	public Officer() {

	}

	public Officer(String name, String surname, String workingDistrict, int officerID, int crimesSolved) {
		super(name, surname);
		this.crimesSolved = crimesSolved;
		this.officerID = officerID;
	}

	public String getName() {
		return this.name;
	}

	public String getSurname() {
		return this.surname;
	}

	public int getOfficerID() {
		return officerID;
	}

	public void setOfficerID(int officerID) {
		this.officerID = officerID;
	}

	public int getCrimesSolved() {
		return crimesSolved;
	}

	public void setCrimesSolved(int crimesSolved) {
		this.crimesSolved = crimesSolved;
	}

	@Override
	public void describe() {

		super.describe();

	}

	@Override
	public String toString() {
		return " Officer ID: " + this.officerID + " Crimes Solved: " + this.crimesSolved;
	}

	int calculateLevel() {
		if (this.crimesSolved < 20)
			return 1;
		else if (this.crimesSolved >= 20 && this.crimesSolved < 40)
			return 2;
		else
			return 3;

	}

}
