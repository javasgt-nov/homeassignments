package january22;

public class Lawyer extends Person {

	private int lawyerID, helpedInCrimesSolving;

	public Lawyer() {

	}

	public Lawyer(String name, String surname, int lawyerID, int helpedInCrimesSolving) {
		super(name, surname);
		this.lawyerID = lawyerID;
		this.helpedInCrimesSolving = helpedInCrimesSolving;
	}

	public String getName() {
		return this.name;
	}

	public String getSurname() {
		return this.surname;
	}

	public int getLawyerID() {
		return lawyerID;
	}

	public void setLawyerID(int lawyerID) {
		this.lawyerID = lawyerID;
	}

	public int getHelpedInCrimesSolving() {
		return helpedInCrimesSolving;
	}

	public void setHelpedInCrimesSolving(int helpedInCrimesSolving) {
		this.helpedInCrimesSolving = helpedInCrimesSolving;
	}

	@Override
	public void describe() {

		super.describe();

	}

	@Override
	public String toString() {
		return " Lawyer ID: " + this.lawyerID + " Helped in crimes solving: "
				+ this.helpedInCrimesSolving;

	}
}
