package december2;

import java.util.*;

public class Converter {

	public static void main(String[] args) {
		
		System.out.println("Enter amount in USD");
		Scanner myScanner = new Scanner(System.in);
		double usd = Double.parseDouble(myScanner.nextLine());
		
		System.out.println("Enter the conversion rate to EUR");
		double rate = Double.parseDouble(myScanner.nextLine());
		
		double eur = usd * rate;
		System.out.println("EUR " + eur);
		
		System.out.println("Enter amount in EUR");
		double eur1 = Double.parseDouble(myScanner.nextLine());
		
		System.out.println("Enter the conversion rate to USD");
		double rate1 = Double.parseDouble(myScanner.nextLine());
		
		double usd1 = eur1 * rate1;
		System.out.println("USD " + usd1);
		
		System.out.println("Enter length in meters");
		double m = Double.parseDouble(myScanner.nextLine());
		
		double ft = m * 3.2808399;
		System.out.println(ft + " feet");
		
		System.out.println("Enter length in feet");
		double ft1 = Double.parseDouble(myScanner.nextLine());
		
		double m1 = ft1 / 3.2808399;
		System.out.println(m1 + " meters");
		
		System.out.println("Enter temperature in Celsius");
		double c = Double.parseDouble(myScanner.nextLine());
		
		double f = c * 9/5 + 32;
		System.out.println(f + " Fahrenheit");
		
		System.out.println("Enter temperature in Fahrenheit");
		double f1 = Double.parseDouble(myScanner.nextLine());
		
		double c1 = (f1 - 32) * 5/9;
		System.out.println(c1 + " Celsius");
		
		System.out.println("Enter temperature in Celsius");
		double c2 = Double.parseDouble(myScanner.nextLine());
		
		double k = c2 + 273.15;
		System.out.println(k + " Kelvin");
		
		System.out.println("Enter temperature in Kelvin");
		double k1 = Double.parseDouble(myScanner.nextLine());
		
		double c3 = k1 - 273.15;
		System.out.println(c3 + " Celsius");
		
		System.out.println("Enter area in square meters");
		double sqm = Double.parseDouble(myScanner.nextLine());
		
		double sqkm = sqm / 1000000;
		System.out.println(sqkm + " km^2");
		
		System.out.println("Enter area in square kilometers");
		double sqkm1 = Double.parseDouble(myScanner.nextLine());
		
		double sqm1 = sqkm1 * 1000000;
		System.out.println(sqm1 + " m^2");
		
		System.out.println("Enter volume in cubic meters");
		double cubm = Double.parseDouble(myScanner.nextLine());
		
		double l = cubm * 1000;
		System.out.println(l + " liters");
		
		System.out.println("Enter volume in liters");
		double l1 = Double.parseDouble(myScanner.nextLine());
		
		double cubm1 = l1 / 1000;
		System.out.println(cubm1 + " m^3");
		
		System.out.println("Enter weight in kilograms");
		double kg = Double.parseDouble(myScanner.nextLine());
		
		double lb = kg / 0.45359237;
		System.out.println(lb + " pounds");
		
		System.out.println("Enter weight in pounds");
		double lb1 = Double.parseDouble(myScanner.nextLine());
		
		double kg1 = lb1 * 0.45259237;
		System.out.println(kg1 + " kg");
		
		System.out.println("Enter time in hours");
		double h = Double.parseDouble(myScanner.nextLine());
		
		double d = h / 24;
		System.out.println(d + " days");
		
		System.out.println("Enter time in days");
		double d1 = Double.parseDouble(myScanner.nextLine());
		
		double h1 = d1 * 24;
		System.out.println(h1 + " hours");
		
		myScanner.close();
		
	}

}
