package december2;

import java.util.*;

public class ConverterFinal {
	
	public static double usd, eur, eur1, usd1, m, ft, ft1, m1, c, f, f1, c1, c2, k, k1, c3, 
	sqm, sqkm, sqkm1, sqm1, cubm, l, l1, cubm1, kg, lb, lb1, kg1, h, d, d1, h1;

	public static void main(String[] args) {
		
		System.out.println("Enter amount in USD");
		Scanner myScanner = new Scanner(System.in);
		ConverterFinal.usd = Double.parseDouble(myScanner.nextLine());
		
		System.out.println("Enter the conversion rate to EUR");
		double rate = Double.parseDouble(myScanner.nextLine());
		
		ConverterFinal.eur = usd * rate;
		
		System.out.println("Enter amount in EUR");
		ConverterFinal.eur1 = Double.parseDouble(myScanner.nextLine());
		
		System.out.println("Enter the conversion rate to USD");
		double rate1 = Double.parseDouble(myScanner.nextLine());
		
		ConverterFinal.usd1 = eur1 * rate1;
		
		System.out.println("Enter length in meters");
		ConverterFinal.m = Double.parseDouble(myScanner.nextLine());
		
		ConverterFinal.ft = m * 3.2808399;
		
		System.out.println("Enter length in feet");
		ConverterFinal.ft1 = Double.parseDouble(myScanner.nextLine());
		
		ConverterFinal.m1 = ft1 / 3.2808399;
		
		System.out.println("Enter temperature in Celsius");
		ConverterFinal.c = Double.parseDouble(myScanner.nextLine());
		
		ConverterFinal.f = c * 9/5 + 32;
		
		System.out.println("Enter temperature in Fahrenheit");
		ConverterFinal.f1 = Double.parseDouble(myScanner.nextLine());
		
		ConverterFinal.c1 = (f1 - 32) * 5/9;
		
		System.out.println("Enter temperature in Celsius");
		ConverterFinal.c2 = Double.parseDouble(myScanner.nextLine());
		
		ConverterFinal.k = c2 + 273.15;
		
		System.out.println("Enter temperature in Kelvin");
		ConverterFinal.k1 = Double.parseDouble(myScanner.nextLine());
		
		ConverterFinal.c3 = k1 - 273.15;
		
		System.out.println("Enter area in square meters");
		ConverterFinal.sqm = Double.parseDouble(myScanner.nextLine());
		
		ConverterFinal.sqkm = sqm / 1000000;
		
		System.out.println("Enter area in square kilometers");
		ConverterFinal.sqkm1 = Double.parseDouble(myScanner.nextLine());
		
		ConverterFinal.sqm1 = sqkm1 * 1000000;
		
		System.out.println("Enter volume in cubic meters");
		ConverterFinal.cubm = Double.parseDouble(myScanner.nextLine());
		
		ConverterFinal.l = cubm * 1000;
		
		System.out.println("Enter volume in liters");
		ConverterFinal.l1 = Double.parseDouble(myScanner.nextLine());
		
		ConverterFinal.cubm1 = l1 / 1000;
		
		System.out.println("Enter weight in kilograms");
		ConverterFinal.kg = Double.parseDouble(myScanner.nextLine());
		
		ConverterFinal.lb = kg / 0.45359237;
		
		System.out.println("Enter weight in pounds");
		ConverterFinal.lb1 = Double.parseDouble(myScanner.nextLine());
		
		ConverterFinal.kg1 = lb1 * 0.45259237;
		
		System.out.println("Enter time in hours");
		ConverterFinal.h = Double.parseDouble(myScanner.nextLine());
		
		ConverterFinal.d = h / 24;
		
		System.out.println("Enter time in days");
		ConverterFinal.d1 = Double.parseDouble(myScanner.nextLine());
		
		ConverterFinal.h1 = d1 * 24;
		
		myScanner.close();
		show();
		
	}

	private static void show() {
		System.out.println(ConverterFinal.usd + " USD = " + ConverterFinal.eur + " EUR");
		System.out.println(ConverterFinal.eur1 + " EUR = " + ConverterFinal.usd1 + " USD");
		System.out.println(ConverterFinal.m + " m = " + ConverterFinal.ft + " ft");
		System.out.println(ConverterFinal.ft1 + " ft = " + ConverterFinal.m1 + " m");
		System.out.println(ConverterFinal.c + " C = " + ConverterFinal.f + " F");
		System.out.println(ConverterFinal.f1 + " F = " + ConverterFinal.c1 + " C");
		System.out.println(ConverterFinal.c2 + " C = " + ConverterFinal.k + " K");
		System.out.println(ConverterFinal.k1 + " K = " + ConverterFinal.c3 + " C");
		System.out.println(ConverterFinal.sqm + " m^2 = " + ConverterFinal.sqkm + " km^2");
		System.out.println(ConverterFinal.sqkm1 + " km^2 = " + ConverterFinal.sqm1 + " m^2");
		System.out.println(ConverterFinal.cubm + " m^3 = " + ConverterFinal.l + " l");
		System.out.println(ConverterFinal.l1 + " l = " + ConverterFinal.cubm1 + " m^3");
		System.out.println(ConverterFinal.kg + " kg = " + ConverterFinal.lb + " lb");
		System.out.println(ConverterFinal.lb1 + " lb = " + ConverterFinal.kg1 + " kg");
		System.out.println(ConverterFinal.h + " h = " + ConverterFinal.d + " days");
		System.out.println(ConverterFinal.d1 + " days = " + ConverterFinal.h1 + " h");
		
	}

}