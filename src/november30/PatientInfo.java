package november30;

import java.util.*;

public class PatientInfo {

	public static String name;
	public static String lastName;
	public static int age;
	public static double weight;
	public static boolean covid19;
	
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Enter Patient's First Name");
		PatientInfo.name = myScanner.nextLine();
		
		System.out.println("Enter Patient's Last Name");
		PatientInfo.lastName = myScanner.nextLine();
		
		System.out.println("Enter Patient's Age");
		PatientInfo.age = Integer.parseInt(myScanner.nextLine());
		
		System.out.println("Enter Patient's Weight");
		PatientInfo.weight = Double.parseDouble(myScanner.nextLine());
		
		System.out.println("Is the Patient Covid-19 positive?");
		PatientInfo.covid19 = Boolean.parseBoolean(myScanner.nextLine());
		myScanner.close();
		show();

	}
	
	public static void show() {
		System.out.println("First Name: " + PatientInfo.name);
		System.out.println("Last Name: " + PatientInfo.lastName);
		System.out.println("Age: " + PatientInfo.age);
		System.out.println("Weight: " + PatientInfo.weight);
		System.out.println("Covid-19 positive: " + PatientInfo.covid19);
		
	}

}
